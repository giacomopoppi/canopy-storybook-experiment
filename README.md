# Canopy Storybook Experiment

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Instructions

1. Follow these instructions to clone the repo locally: https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html
2. Install Node.JS (If not already installed): https://nodejs.org/en/download/
2. Run `npm install`. This will install required dependencies
3. Run `npm start` - This runs the dummy app in the development mode. Open [http://localhost:3000](http://localhost:3000) to view it in your browser.
4. Run `npm run storybook` - Runs storybook. Open [http://localhost:6006](http://localhost:6006) to view it in your browser.
