import logo from './logo.svg';
import './App.css';
import {Page} from './stories/Page'
import {Button} from './stories/Button'

function App() {
  return (
    <div className="App">
      <Page>
        
      </Page>
      <Button
        label="Example Button"
        onClick={() => {}}
        primary
      />
    </div>
  );
}

export default App;
